package com.itcast.test;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * create by IntelliJ IDEA
 * User: HuangRZ
 * QQ: 917647409
 * Email: huangrz11@163.com
 * Date: 2018/2/15 0015
 * Time: 3:16
 * version: 1.0
 * Description:
 **/
public class CrudTest {

    private Logger logger = Logger.getLogger(CrudTest.class);

    @Autowired
    private TransportClient client;

    public void get() {
        GetResponse getResponse = this.client.prepareGet(index, type, id).get();
        logger.info(getResponse.getSource());
    }

    public void update() {
        try {
            XContentBuilder builder = XContentFactory.jsonBuilder().startObject();
            if (StringUtils.isNotEmpty(title)) {
                builder.field("title", title);
            }
            if (StringUtils.isNotEmpty(author)) {
                builder.field("author", author);
            }
            UpdateResponse updateResponse = this.client.prepareUpdate(index, type, id)
                    .setDoc(builder.endObject()).get();
            return new ResponseEntity<>(updateResponse, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        DeleteResponse response = this.client.prepareDelete(index, type, id).get();
        return new ResponseEntity<>(response.getResult(), HttpStatus.OK);
    }

    public void add() {
        try {
            XContentBuilder builder = XContentFactory.jsonBuilder()
                    .startObject()
                    .field("title", title)
                    .field("author", author)
                    .field("word_count", wordCount)
                    .field("publish_date", publishDate)
                    .endObject();
            IndexResponse indexResponse = this.client.prepareIndex(index, type)
                    .setSource(builder).get();
            return new ResponseEntity<>(indexResponse.getResult(), HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void search(){
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (StringUtils.isNotEmpty(author)) {
            boolQuery.must(QueryBuilders.matchQuery("author", author));
        }
        if (StringUtils.isNotEmpty(title)) {
            boolQuery.must(QueryBuilders.matchQuery("title", title));
        }
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("word_count").from(gtWordCount);
        if (ltWordCount != null && ltWordCount > 0) {
            rangeQueryBuilder.to(ltWordCount);
        }
        boolQuery.filter(rangeQueryBuilder);
        SearchRequestBuilder builder = this.client.prepareSearch(index)
                .setTypes(type)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(boolQuery)
                .setFrom(0)
                .setSize(10);
        logger.info(builder);
        List<Map<String, Object>> result = new ArrayList<>();
        for (SearchHit hit : builder.get().getHits()) {
            result.add(hit.getSourceAsMap());
            logger.info(hit.getSourceAsMap());
        }
    }
}
