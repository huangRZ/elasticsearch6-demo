package com.itcast.test;

import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class Es6DemoTests {

    @Autowired
    private TransportClient client;

    @Test
    public void contextLoads() {

    }

    /***
     * 判断索引是否存在
     */
    @Test
    public void isIndexExist() {
        //获取IndicesAdminClient客户端（Administrative actions/operations against indices）
        IndicesAdminClient indicesAdminClient = client.admin().indices();
        //判断索引是否存在
        IndicesExistsResponse existsResponse = indicesAdminClient.prepareExists("book").get();
        System.out.println(existsResponse.isExists());
    }

    /**
     * 非结构化创建索引
     */
    @Test
    public void createIndex() {
        IndicesAdminClient indicesAdminClient = client.admin().indices();
        CreateIndexResponse indexResponse = indicesAdminClient.prepareCreate("people").get();
        System.out.println(indexResponse.index());
    }

    /**
     * 结构化创建索引
     */
    @Test
    public void createIndexByMapping() throws IOException {
        Settings settings = Settings.builder()
                .put("number_of_shards", 3)
                .put("number_of_replicas", 1)
                .build();

//        CreateIndexResponse people = client.admin().indices()
//                .prepareCreate("people")
//                .setSettings(settings)
//                .get();
//        System.out.println(people.isShardsAcknowledged());

        XContentBuilder mapping = XContentFactory.jsonBuilder()
                .startObject()
                .startObject("student")
                .startObject("properties")
                .startObject("id")
                .field("type", "long")
                .endObject()
                .startObject("title")
                .field("type", "text")
//				.field("analyzer", "ik_max_word")
//				.field("search_analyzer", "ik_max_word")
                .field("boost", 2)
                .endObject()
                .startObject("content")
                .field("type", "text")
//				.field("analyzer", "ik_max_word")
//				.field("search_analyzer", "ik_max_word")
                .endObject()
                .startObject("postdate")
                .field("type", "date")
                .field("format", "yyyy-MM-dd HH:mm:ss")
                .endObject()
                .startObject("url")
                .field("type", "keyword")
                .endObject()
                .endObject()
                .endObject()
                .endObject();
        System.out.println(mapping.string());

        PutMappingResponse response = client.admin().indices().preparePutMapping("people")
                .setType("student")
                .setSource(mapping.string(), XContentType.JSON)
                .get();
        System.out.println(response);
    }

    @Test
    public void addIndex() throws IOException {
        XContentBuilder builder1 = XContentFactory.jsonBuilder().startObject()
                .field("title", "标题1")
                .field("author", "作者1")
                .field("word_count", 100001)
                .field("publish_date", "2018-02-25")
                .endObject();

        XContentBuilder builder2 = XContentFactory.jsonBuilder().startObject()
                .field("title", "标题2")
                .field("author", "作者2")
                .field("word_count", 100002)
                .field("publish_date", "2018-02-26")
                .endObject();
        IndexResponse indexResponse = this.client.prepareIndex("book", "novel")
                .setSource(builder1)
                .setSource(builder2)
                .get();
        System.out.println(indexResponse.getResult());
    }
}
